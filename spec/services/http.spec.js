const HttpService = require("../../lib/api/services/http").HttpService;
const expect = require("chai").expect;
const sinon = require("sinon");

describe("HttpService", () => {
	const baseUrl = "http://example.com";
	let http;
	let request;

	beforeEach(() => {
		request = {
			get: () => {}
		};
		http = new HttpService(request, baseUrl);
	});

	describe("on a GET request", () => {
		it("should resolve to a response body on success", () => {
			// given
			const url = "/foo";
			const response = {foo: "bar"};

			sinon.stub(request, "get")
				.withArgs(baseUrl + url, sinon.match.any)
				.callsFake((url, callback) => {
					callback(undefined, undefined, JSON.stringify(response));
				});

			// when
			return http.get(url)
				.then(result => {
					expect(result).to.deep.equal(response);
				});
		});

		it("should be successfully rejected on error", () => {
			// given
			let errorMessage = "Foo";
			sinon.stub(request, "get").callsFake((url, callback) => {
				callback(errorMessage, undefined, undefined);
			});

			// when
			return http.get("/foo")
				.then(() => {
					throw new Error("Expected the promise to be rejected.");
				})
				.catch(error => {
					expect(error).to.equal(errorMessage);
				});
		});
	});
});
