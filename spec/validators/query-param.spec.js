const expect = require("chai").expect;
const sinon = require("sinon");
const queryParamValidator = require("../../lib/api/validators/query-param").queryParamValidator;

describe("queryParamValidator", () => {
	it("should call next with no arguments if param is present", () => {
		const req = {query: {foo: "Bar"}};
		const next = sinon.spy();
		queryParamValidator("foo")(req, undefined, next);

		expect(next.called).to.be.true;
		expect(next.getCall(0).args.length).to.equal(0);
	});

	it("should call next with error if param is not present", () => {
		const req = {query: {}};
		const next = sinon.spy();
		queryParamValidator("foo")(req, undefined, next);

		expect(next.called).to.be.true;
		expect(next.getCall(0).args[0].message).to.equal("Missing foo query parametre.");
	});

	it("should call next with error if param is bresent, but empty", () => {
		const req = {query: {foo: ""}};
		const next = sinon.spy();
		queryParamValidator("foo")(req, undefined, next);

		expect(next.called).to.be.true;
		expect(next.getCall(0).args[0].message).to.equal("Missing foo query parametre.");
	});
});

