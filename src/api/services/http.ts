import * as request from "request";

export class HttpService {

	private request: any;
	private baseUrl: string;

	public constructor(request: any, baseUrl: string) {
		this.request = request;
		this.baseUrl = baseUrl;
	}

	public get(url: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.request.get(this.baseUrl + url, (error: string, response: any, body: string) => {
				if (error) {
					reject(error);
				}

				resolve(JSON.parse(body));
			});
		});
	}
}

export const http: HttpService = new HttpService(request, require("../../../config/config.json").baseUrl);
