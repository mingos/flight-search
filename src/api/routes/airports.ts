import * as express from "express";
import {http} from "../services/http";
import {queryParamValidator} from "../validators/query-param";
import * as queryString from "query-string";

export const airportsRouter: express.Router = express.Router();

airportsRouter.get("/", [
	queryParamValidator("q"),
	(req: express.Request, res: express.Response, next: Function) => {
	const qs: string = queryString.stringify({
		q: req.query.q
	});
		http.get(`/airports?${qs}`)
			.then(body => {
				res.send(body);
			})
			.catch(err => {
				next(new Error(err));
			});
	}
]);
