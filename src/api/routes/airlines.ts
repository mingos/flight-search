import * as express from "express";
import {http} from "../services/http";

export const airlinesRouter: express.Router = express.Router();

airlinesRouter.get("/", (req: express.Request, res: express.Response, next: Function) => {
	http.get("/airlines")
		.then(body => {
			res.send(body);
		})
		.catch(err => {
			next(new Error(err));
		});
});
