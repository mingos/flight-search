import * as express from "express";
import {http} from "../services/http";
import {queryParamValidator} from "../validators/query-param";
import * as queryString from "query-string";

export const searchRouter: express.Router = express.Router();

searchRouter.get("/", [
	queryParamValidator("from"),
	queryParamValidator("to"),
	queryParamValidator("date"),
	(req: express.Request, res: express.Response, next: Function) => {
		http.get("/airlines")
			.then((airlines: Array<any>) => {
				const requests: Array<Promise<any>> = [];
				const qs: string = queryString.stringify({
					from: req.query.from,
					to: req.query.to,
					date: req.query.date
				});

				airlines.forEach(airline => {
					requests.push(http.get(`/flight_search/${airline.code}?${qs}`));
				});

				return Promise.all(requests);
			})
			.then((flightsSets: Array<Array<any>>) => {
				const results: Array<any> = [];

				flightsSets.forEach(flights => {
					flights.forEach(flight => results.push(flight));
				});

				res.send(results);
			})
			.catch((err: string) => {
				next(new Error(err));
			});
	}
]);
