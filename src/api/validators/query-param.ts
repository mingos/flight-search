import {Request, Response} from "express";

export function queryParamValidator(name: string): (req: Request, res: Response, next: Function) => void {
	return (req: Request, res: Response, next: Function) => {
		if (!(name in req.query) || !req.query[name]) {
			next(new Error(`Missing ${name} query parametre.`));
		} else {
			next();
		}
	};
}
