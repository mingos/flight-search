import * as express from "express";
import * as bodyParser from "body-parser";
import * as path from "path";
import {airlinesRouter} from "./routes/airlines";
import {airportsRouter} from "./routes/airports";
import {searchRouter} from "./routes/search";

const port: number = process.env.PORT || 3000;
const app: express.Express = express();

app.use(express.static(path.join(__dirname, "../../public")));
app.use(bodyParser.json());

app.use("/airlines", airlinesRouter);
app.use("/airports", airportsRouter);
app.use("/search", searchRouter);

app.use((err: any, req: express.Request, res: express.Response, next: Function) => {
	res.status(err.status || 500);
	res.setHeader("Content-Type", "application/json");
	res.send({
		message: err.message,
		status: err.status,
		stack: err.stack
	});
});

app.listen(port, () => {
	console.log(`Magic happens on port ${port}`);
});
