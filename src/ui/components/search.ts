import {HttpService} from "../services/http";
import * as queryString from "query-string";
import Awesomplete = require("awesomplete");
import {FlightsComponent} from "./flights";
import {TabsComponent} from "./tabs";
import * as moment from "moment";
import {eventBus} from "../services/event-bus";

export class SearchComponent {
	private http: HttpService;
	private fromElement: HTMLInputElement;
	private toElement: HTMLInputElement;
	private dateElement: HTMLInputElement;
	private submitElement: HTMLButtonElement;
	private flightsComponent: FlightsComponent;
	private tabsComponent: TabsComponent;

	private submitText: string;

	public constructor(http: HttpService, flightsComponent: FlightsComponent, tabsComponent: TabsComponent) {
		this.http = http;
		this.fromElement = <HTMLInputElement>document.getElementById("from");
		this.toElement = <HTMLInputElement>document.getElementById("to");
		this.dateElement = <HTMLInputElement>document.getElementById("date");
		this.submitElement = <HTMLButtonElement>document.getElementById("search");
		this.flightsComponent = flightsComponent;
		this.tabsComponent = tabsComponent;
		this.submitText = this.submitElement.textContent;

		const fromAwesomplete: Awesomplete = new Awesomplete(this.fromElement);
		const toAwesomplete: Awesomplete = new Awesomplete(this.toElement);

		eventBus.subscribe("dateChanged", (date: string) => {
			if (this.dateElement.value !== date) {
				this.dateElement.value = date;
				this.initiateSearch();
			}
		});

		this.fromElement.addEventListener("input", () => {
			const value = this.fromElement.value;
			setTimeout(() => {
				if (value !== this.fromElement.value || value.length < 2) {
					return;
				}
				const qs: string = queryString.stringify({
					q: value
				});
				this.http.get(`/airports?${qs}`)
					.then((airports: Array<any>) => {
						fromAwesomplete.list = airports.map(item => {
							return {label: item.airportName, value: item.airportCode};
						});
						fromAwesomplete.evaluate();
					});
			}, 500);
		});

		this.toElement.addEventListener("input", () => {
			const value = this.toElement.value;
			setTimeout(() => {
				if (value !== this.toElement.value  || value.length < 2) {
					return;
				}
				const qs: string = queryString.stringify({
					q: value
				});
				this.http.get(`/airports?${qs}`)
					.then((airports: Array<any>) => {
						toAwesomplete.list = airports.map(item => {
							return {label: item.airportName, value: item.airportCode};
						});
						toAwesomplete.evaluate();
					});
			}, 500);
		});
		this.submitElement.addEventListener("click", () => {
			this.initiateSearch();
		});
	}

	private initiateSearch() {
		this.flightsComponent.flights = [];
		this.submit().then(flights => {
			this.tabsComponent.setDate(moment(new Date(this.dateElement.value)));
			this.flightsComponent.flights = flights;
		});
	}

	private submit(): Promise<Array<any>> {
		this.setSearchingState();

		return this.getFlights(this.fromElement.value, this.toElement.value, this.dateElement.value)
			.then((flights) => {
				this.unsetSearchingState();
				return flights;
			})
			.catch(() => {
				this.unsetSearchingState();
			});
	}

	private getFlights(from: string, to: string, date: string): Promise<Array<any>> {
		const qs: string = queryString.stringify({from, to, date});
		return this.http.get(`/search?${qs}`);
	}

	private setSearchingState(): void {
		this.submitElement.disabled = true;
		this.submitElement.innerHTML = '<span class="fa fa-circle-o-notch fa-spin">';
	}

	private unsetSearchingState(): void {
		this.submitElement.disabled = false;
		this.submitElement.textContent = this.submitText;
	}
}
