import * as nunjucks from "nunjucks";
import {Template} from "nunjucks";
import * as moment from "moment";

export class FlightsComponent {

	private _flights: Array<any>;
	private parent: HTMLElement;
	private flightsTemplate: Template;
	private wrapperTemplate: Template;

	public constructor() {
		this.parent = document.getElementById("flights");
		this.flightsTemplate = nunjucks.compile(document.getElementById("flights-template").innerHTML);
		this.wrapperTemplate = nunjucks.compile(document.getElementById("flights-wrapper-template").innerHTML);
	}

	public get flights() {
		return this._flights;
	}

	public set flights(flights: Array<any>) {
		this._flights = flights.sort((a, b) => a.price - b.price).map(flight => {
			// disregard local timezone offset. JS makes everything local and I don't feel like struggling with this.
			flight.start.dateTime = moment(flight.start.dateTime).format("YYYY-MM-DD HH:mm");
			flight.finish.dateTime = moment(flight.finish.dateTime).format("YYYY-MM-DD HH:mm");
			return flight;
		});
		this.clearDisplay();
		if (this.flights.length) {
			this.displayFlights();
		}
	}

	public clearDisplay(): void {
		while (this.parent.firstChild) {
			this.parent.removeChild(this.parent.firstChild);
		}
	}

	public displayFlights(): void {
		const flights = this.flightsTemplate.render({flights: this.flights});

		this.parent.innerHTML = this.wrapperTemplate.render({flights});
	}
}
