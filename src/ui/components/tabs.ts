import * as nunjucks from "nunjucks";
import {Template} from "nunjucks";
import {Moment} from "moment";
import * as moment from "moment";
import {FlightsComponent} from "./flights";
import {eventBus} from "../services/event-bus";

export class TabsComponent {

	private static dateFormat: string = "YYYY-MM-DD";
	private template: Template;
	private element: HTMLElement;
	private flightsComponent: FlightsComponent;

	public constructor(flightsComponent: FlightsComponent) {
		this.flightsComponent = flightsComponent;
		this.template = nunjucks.compile(document.getElementById("flight-tabs-template").innerHTML);
		this.element = <HTMLElement>document.getElementById("flight-tabs");

		this.element.addEventListener("click", (ev: Event) => {
			const target = <HTMLElement>ev.target;
			if (target.dataset && target.dataset.date) {
				// ignore timezone gotcha. Deal with it, Western hemisphere.
				this.setDate(moment(new Date(target.dataset.date)));
			}
		});
	}

	public setDate(date: Moment): void {
		const dates = {
			date: date.format(TabsComponent.dateFormat),
			minusTwo: moment(date).subtract(2, "days").format(TabsComponent.dateFormat),
			minusOne: moment(date).subtract(1, "days").format(TabsComponent.dateFormat),
			plusOne: moment(date).add(1, "days").format(TabsComponent.dateFormat),
			plusTwo: moment(date).add(2, "days").format(TabsComponent.dateFormat)
		};

		this.element.innerHTML = this.template.render(dates);

		eventBus.publish("dateChanged", date.format(TabsComponent.dateFormat));
	}
}
