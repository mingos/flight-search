import {SearchComponent} from "./components/search";
import {http} from "./services/http";
import {FlightsComponent} from "./components/flights";
import {TabsComponent} from "./components/tabs";

const flightsComponent = new FlightsComponent();
const tabsComponent = new TabsComponent(flightsComponent);
new SearchComponent(http, flightsComponent, tabsComponent);

