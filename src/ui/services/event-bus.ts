class EventBus {
	private subscriptions: any;

	public constructor() {
		this.subscriptions = {};
	}

	public subscribe(type: string, callback: Function) {
		this.subscriptions[type] = this.subscriptions[type] || [];
		this.subscriptions[type].push(callback);
	}

	public publish(type: string, data?: any) {
		this.subscriptions[type].forEach((callback: Function) => {
			callback(data);
		});
	}
}

export const eventBus = new EventBus();
