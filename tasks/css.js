const gulp = require("gulp");
const sass = require("gulp-sass");
const cleanCSS = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const merge = require("merge-stream");

gulp.task("css", () => {
	const scssStream = gulp.src("styles/index.scss").pipe(sass());
	const cssStream = gulp.src("node_modules/awesomplete/awesomplete.css");

	return merge(scssStream, cssStream)
		.pipe(concat("style.css"))
		.pipe(autoprefixer({
			browsers: ["last 2 versions"],
			cascade: false
		}))
		.pipe(cleanCSS())
		.pipe(gulp.dest("public/css"));
});

