const gulp = require("gulp");
const runSequence = require("run-sequence");

gulp.task("rebuild:api", () => runSequence("ts:api", "test"));
gulp.task("rebuild:ui", () => runSequence("ts:ui", "test"));
gulp.task("rebuild", () => runSequence(["ts:api", "ts:ui"], "test"));
