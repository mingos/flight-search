const gulp = require("gulp");
const copy = require("gulp-copy");

gulp.task("assets", () => {
	return gulp.src("node_modules/font-awesome/fonts/*")
		.pipe(copy("public", {prefix: 2}))
		.pipe(gulp.dest("public"));
});
