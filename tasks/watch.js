const gulp = require("gulp");

gulp.task("watch", function() {
	gulp.watch(["src/api/**/*.ts"], ["rebuild:api"]);
	gulp.watch(["src/ui/**/*.ts"], ["rebuild:ui"]);
	gulp.watch(["spec/**/*"], ["test"]);
	gulp.watch(["styles/**/*.scss"], ["css"]);
});
