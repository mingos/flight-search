const gulp = require("gulp");
const mocha = require("gulp-mocha");

gulp.task("test", function() {
	return gulp.src("spec/**/*.spec.js")
		.pipe(mocha());
});
