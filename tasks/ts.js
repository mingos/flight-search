const gulp = require("gulp");
const ts = require("gulp-typescript");
const merge = require("merge-stream");
const browserify = require("browserify");
const tsify = require("tsify");
const source = require("vinyl-source-stream");


gulp.task("ts:api", () => {
	const config = require("../tsconfig-api.json").compilerOptions;
	const result = gulp.src([config.rootDir + "/**/*.ts"])
		.pipe(ts(config));

	return merge(
		result.dts.pipe(gulp.dest("types/")),
		result.js.pipe(gulp.dest(config.outDir))
	);
});

gulp.task("ts:ui", () => {
	const config = require("../tsconfig-ui.json").compilerOptions;

	const bundler = browserify({entries: config.rootDir + "/index.ts", debug: false})
		.add(config.rootDir + "/index.ts")
		.plugin(tsify, config);

	return bundler.bundle()
		.pipe(source("app.js"))
		.pipe(gulp.dest(config.outDir));
});
